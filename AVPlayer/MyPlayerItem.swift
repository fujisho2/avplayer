//
//  MyPlayerItem.swift
//  AVPlayer
//
//  Created by fujisho on 2015/08/29.
//  Copyright (c) 2015年 Shotaro Fujie. All rights reserved.
//

import Cocoa
import AVFoundation

class MyPlayerItem: AVPlayerItem {
    var lapList: [LapList] = []
    var artist: String!
    var title: String!
    var isPlaying = false
}

class LapList {
    var begin: Double!
    var end: Double!
    
    init() {
        self.begin = 0.0
        self.end = 0.0
    }
}
