//
//  MainWindowController.swift
//  AVPlayer
//
//  Created by fujisho on 2015/08/27.
//  Copyright (c) 2015年 Shotaro Fujie. All rights reserved.
//

import Cocoa
import AVFoundation

class MainWindowController: NSWindowController, NSTableViewDataSource, NSTableViewDelegate {

    var timeObserver: AnyObject!
    var timer: NSTimer!
    
    var playerItem: [MyPlayerItem] = []
    var audioPlayer: AVPlayer!
    var selectNumber: Int = -1 {
        didSet {
            lapTableView.reloadData()
        }
    }
    var playNumber = -1 {
        didSet {
            if oldValue != -1 {
                playerItem[oldValue].isPlaying = false
            }
            playerItem[playNumber].isPlaying = true
            trackTableView.reloadData()
        }
    }
    var isLapMode: Bool = false {
        didSet {
            if timer != nil {
                if timer.valid {
                    timer.invalidate()
                }
            }
        }
    }

    @IBOutlet weak var seekBar: NSSlider!
    @IBOutlet weak var lapButton: NSButton!
    @IBOutlet weak var lapTableView: NSTableView!
    @IBOutlet weak var trackTableView: NSTableView!
    @IBOutlet weak var playButton: NSButton!
    
    override var windowNibName: String! {
        return "MainWindowController"
    }
    
    override func windowDidLoad() {
        super.windowDidLoad()

        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        seekBar.minValue = 0
        seekBar.target = self
        seekBar.action = Selector("onSliderValueChange:")
        
        lapTableView.target = self
        trackTableView.target = self
        lapTableView.doubleAction = Selector("lapTableViewDoubleClicked")
        trackTableView.doubleAction = Selector("trackTableViewDoubleClicked")
    }
    
    // MARK: - Actions
    
    @IBAction func loadSoundOpenPanel(sender: AnyObject) {
        var openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = true
        openPanel.canChooseDirectories = true
        openPanel.canCreateDirectories = true
        openPanel.canChooseFiles = true
        openPanel.beginWithCompletionHandler { (result) -> Void in
            if result == NSFileHandlingPanelOKButton {
                //Do what you will
                //If there's only one URL, surely 'openPanel.URL'
                //but otherwise a for loop works

                
                for url in openPanel.URLs {
                    let asset = AVURLAsset(URL: url as! NSURL, options: nil)
                    let tmpPlayerItem = MyPlayerItem(asset: asset)
                    let metadata : Array = asset.commonMetadata
                        
                    for item in metadata {
                        switch item.commonKey as String {
                        case AVMetadataCommonKeyTitle:
                            // タイトル取得
                            if item.stringValue == nil {
                                tmpPlayerItem == url.lastPathComponent
                            } else {
                                tmpPlayerItem.title = item.stringValue
                            }
                        case AVMetadataCommonKeyAlbumName:
                            // アルバム名取得
                            println(item.stringValue)
                        case AVMetadataCommonKeyArtist:
                            // アーティスト名取得
                            println(item.stringValue)
                            if item.stringValue == nil {
                                tmpPlayerItem.artist = ""
                            } else {
                                tmpPlayerItem.artist = item.stringValue
                            }
                        
                        case AVMetadataCommonKeyArtwork:
                            // アートワーク取得
                            println("end")
                        default:
                            break
                        }
                    }
                    self.playerItem.append(tmpPlayerItem)
                }
                self.trackTableView.reloadData()
            }
        }
    }
    
    @IBAction func lapButtonPushed(sender: NSButton) {

        if playNumber < 0 || audioPlayer == nil {
            return
        }
        
        if sender.state == NSOnState {
            // first push
            sender.title = "End"
            let beginTime = CMTimeGetSeconds(audioPlayer.currentTime())
            var lapList = LapList()
            lapList.begin = beginTime
            playerItem[playNumber].lapList.append(lapList)
            
        } else {
            // second push
            sender.title = "Begin"
            let endTime = CMTimeGetSeconds(audioPlayer.currentTime())
            playerItem[playNumber].lapList[playerItem[playNumber].lapList.count - 1].end = endTime
            lapTableView.reloadData()
        }
    }
    
    
    @IBAction func playButtonPushed(sender: NSButton) {
        if selectNumber == -1 {
            sender.title = "Play"
            return
        }
        
        isLapMode = false
        
        if sender.state == NSOnState {
            sender.title = "Pause"
            if audioPlayer == nil {
                play(selectNumber)
            } else {
                audioPlayer.play()
            }

        } else {
            sender.title = "Play"
            if audioPlayer != nil {
                audioPlayer.pause()
            }
        }

    }
    
    @IBAction func nextButtonPushed(sender: NSButton) {
        if audioPlayer == nil || playNumber == -1 {
            return
        }
        
        isLapMode = false
        
        if playNumber + 1 < playerItem.count {
            play(playNumber + 1)
        } else {
            // TODO: - repeat
        }
    }
    
    @IBAction func prevButtonPushed(sender: NSButton) {
        if audioPlayer == nil || playNumber == -1 {
            return
        }
        isLapMode = false
        
        if playNumber > 0 {
            if CMTimeGetSeconds(audioPlayer.currentTime()) < 1.5 {
                play(playNumber - 1)
            } else {
                audioPlayer.seekToTime(CMTimeMakeWithSeconds(0, Int32(NSEC_PER_SEC)))
            }
        } else {
            audioPlayer.seekToTime(CMTimeMakeWithSeconds(0, Int32(NSEC_PER_SEC)))
        }

        audioPlayer.play()
    }
    
    // MARK: - NSTableView

    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        switch tableView.tag {
        case 0:
            return playerItem.count
        case 1:
            if selectNumber >= 0 {
                return playerItem[selectNumber].lapList.count
            } else {
                return 0
            }
        default:
            return 0
        }
    }
    
    
    func tableView(tableView: NSTableView, objectValueForTableColumn tableColumn: NSTableColumn?, row: Int) -> AnyObject? {
        switch tableView.tag {
        case 0:
            if tableColumn?.identifier == "info" {
                let string = "\(playerItem[row].title) - \(playerItem[row].artist)"
                return string
            } else {
                if playerItem[row].isPlaying {
                    return "●"
                } else {
                    return row
                }
            }

        case 1:
            if selectNumber >= 0 {
                let string = NSString(format: "%.2f - %.2f", playerItem[selectNumber].lapList[row].begin, playerItem[selectNumber].lapList[row].end)
                return string
            } else {
                return nil
            }
        default:
            return nil
        }
    }
    
    func tableView(tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        switch tableView.tag {
        case 0:
            selectNumber = row
            break;
        case 1:
            
            break;
        default:
            break;
        }

        return true
    }
    
    func lapTableViewDoubleClicked() {
        let row = lapTableView.clickedRow
        if row < 0 {
            return
        }
        let beginTime = playerItem[selectNumber].lapList[row].begin
        let endTime   = playerItem[selectNumber].lapList[row].end
        isLapMode = true
        audioPlayer.pause()
        audioPlayer.replaceCurrentItemWithPlayerItem(playerItem[selectNumber])
        audioPlayer.seekToTime(CMTimeMakeWithSeconds(beginTime, Int32(NSEC_PER_SEC)))
        audioPlayer.play()
        timer = NSTimer.scheduledTimerWithTimeInterval(endTime - beginTime, target: self, selector: "timerRepeatOn:", userInfo: ["begin":beginTime], repeats: true)
    }
    
    func trackTableViewDoubleClicked() {
        let row = trackTableView.clickedRow
        if row < 0 {
            return
        }
        isLapMode = false
        playButton.title = "Pause"
        playButton.state = NSOnState

        play(row)
    }
    
    
    func timerRepeatOn(timer: NSTimer){
        let userInfo = timer.userInfo as! Dictionary<String, Double>
        let begin = userInfo["begin"]
        audioPlayer.seekToTime(CMTimeMakeWithSeconds(begin!, Int32(NSEC_PER_SEC)))
    }
    
    
    // MARK: - Player functions
    
    func onSliderValueChange(sender : NSSlider){
        
        println("bar touch")
        // 動画の再生時間をシークバーとシンクロさせる.
        let time = CMTimeMakeWithSeconds(Double(sender.doubleValue), Int32(NSEC_PER_SEC))
        audioPlayer.seekToTime(time)
        
        
    }
    
    func playWithNumber(number: Int) {
        playNumber = number
        audioPlayer = AVPlayer(playerItem: playerItem[number])
        audioPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        seekBar.maxValue = Double(CMTimeGetSeconds(playerItem[number].asset.duration))
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "itemDidFinishedPlaying:", name: AVPlayerItemDidPlayToEndTimeNotification, object: playerItem[number])
        let interval : Double = Double(0.5 * seekBar.maxValue) / Double(seekBar.bounds.maxX)
        
        let time : CMTime = CMTimeMakeWithSeconds(interval, Int32(NSEC_PER_SEC))
        self.timeObserver = audioPlayer.addPeriodicTimeObserverForInterval(time, queue: nil) { (time) -> Void in
            
            let duration = CMTimeGetSeconds(self.audioPlayer.currentItem.duration)
            let time = CMTimeGetSeconds(self.audioPlayer.currentTime())
            let value = Double(self.seekBar.maxValue - self.seekBar.minValue) * Double(time) / Double(duration) + Double(self.seekBar.minValue)
            self.seekBar.doubleValue = value
            
        }
        audioPlayer.play()
    }
    
    func play(num: Int) {
        playNumber = num
        
        if audioPlayer == nil {
            audioPlayer = AVPlayer(playerItem: playerItem[num])
        } else {
            audioPlayer.seekToTime(CMTimeMakeWithSeconds(0, Int32(NSEC_PER_SEC)))
            audioPlayer.replaceCurrentItemWithPlayerItem(playerItem[num])
        }
        audioPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        seekBar.maxValue = Double(CMTimeGetSeconds(playerItem[num].asset.duration))
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "itemDidFinishedPlaying:", name: AVPlayerItemDidPlayToEndTimeNotification, object: playerItem[num])
        let interval : Double = Double(0.5 * seekBar.maxValue) / Double(seekBar.bounds.maxX)
        
        let time : CMTime = CMTimeMakeWithSeconds(interval, Int32(NSEC_PER_SEC))
        self.timeObserver = audioPlayer.addPeriodicTimeObserverForInterval(time, queue: nil) { (time) -> Void in
            
            let duration = CMTimeGetSeconds(self.audioPlayer.currentItem.duration)
            let time = CMTimeGetSeconds(self.audioPlayer.currentTime())
            let value = Double(self.seekBar.maxValue - self.seekBar.minValue) * Double(time) / Double(duration) + Double(self.seekBar.minValue)
            self.seekBar.doubleValue = value
            
        }
        audioPlayer.play()
        
    }
    
    func itemDidFinishedPlaying(notification: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
        if playNumber + 1 < playerItem.count {
            play(playNumber + 1)
        } else {
            // TODO: - repeat
        }
    }
    

    
   
}
