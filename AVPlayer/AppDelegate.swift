//
//  AppDelegate.swift
//  AVPlayer
//
//  Created by fujisho on 2015/08/27.
//  Copyright (c) 2015年 Shotaro Fujie. All rights reserved.
//

import Cocoa
import AVFoundation

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

        var mainWindowController: MainWindowController?


    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
        // Create a window controller
        let mainWindowController = MainWindowController()
        // Put the window of the window controller on screen
        mainWindowController.showWindow(self)
        // Set the property to point to the window controller
        self.mainWindowController = mainWindowController
        
//        
//        var fileUrl = NSURL(fileURLWithPath: "/Users/fujisho/Desktop/music.m4a")
//        var asset = AVURLAsset(URL: fileUrl, options: nil)
//        
//        var playerItem = MyPlayerItem(asset: asset)
//        
//        var player = AVPlayer(playerItem: playerItem)
//        
//        fileUrl =
//        
//        
//        player = AVPlayer(playerItem: playerItem)
//
//        playerItem.lapList.append(LapList())
//        playerItem.lapList[0].begin = 10.0
//        playerItem.lapList[0].end   = 12.0
    }
    
    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

